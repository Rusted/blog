<?php

namespace AppBundle\Controller;

use AppBundle\Command\Command\PostCreateCommand;
use AppBundle\Command\Exception\CommandHandlerException;
use AppBundle\Command\Handler\PostCreateHandler;
use AppBundle\Voter\PostVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PostApiController extends Controller
{
    /**
     * @Route("/api", name="api_home")
     * @param Request $request
     * @return Response
     */
    public function homeAction(Request $request)
    {
        return new Response("Blog API");
    }

    /**
     * @Route("/api/post/create", name="api_post_create")
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $this->denyAccessUnlessGranted(PostVoter::POST_CREATE);

        $content = $request->getContent();
        $values = json_decode($content, true); // 2nd param to get as array

        if (!is_array($values))
        {
           return new JsonResponse('Bad JSON format');
        }

        $command = $this->createCreatePostCommandFromArray($values);

        /** @var PostCreateHandler $handler */
        $handler = $this->get('app.handler.post.create');
        try {
            $handler->handle($command);
            return new JsonResponse('Success');
        } catch (CommandHandlerException $e) {
            $messages = implode("; ", $e->getMessages());

            return new JsonResponse($messages);
        }
    }

    /**
     * @param array $values
     * @return PostCreateCommand
     */
    private function createCreatePostCommandFromArray($values)
    {
        $command = new PostCreateCommand();
        $command->user = $this->getUser();
        $command->body = $values['body'];
        $command->status = $values['status'];
        $command->title = $values['title'];

        return $command;
    }
}