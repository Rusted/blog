<?php

namespace AppBundle\Model;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Repository\PostRepository;
use Doctrine\ORM\EntityManager;

class PostModel
{
    /** @var PostRepository */
    private $postRepository;

    /** @var EntityManager */
    private $entityManager;

    /**
     * PostModel constructor.
     * @param PostRepository $postRepository
     * @param EntityManager $entityManager
     */
    public function __construct(PostRepository $postRepository, EntityManager $entityManager)
    {
        $this->postRepository = $postRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param User $author
     * @param string $title
     * @param string $body
     * @param integer $status
     * @param \DateTime $published
     * @return Post
     */
    public function create(User $author, $title, $body, $status, $published = null)
    {
        $post = new Post();
        $post->setAuthor($author);
        $post->setTitle($title);
        $post->setBody($body);
        $post->setStatus($status);
        if ($published instanceof \DateTime) {
            $post->setPublished($published);
        }
        
        $this->save($post);
        return $post;
    }

    /**
     * @param User $author
     * @param \DateTime $date
     * @return array
     */
    public function getUserPostsPerDay(User $author, \DateTime $date)
    {
        return $this->postRepository->findBy(['author' => $author, 'createdAt' => $date]);
    }

    /**
     * @param Post $post
     */
    private function save(Post $post)
    {
        $this->entityManager->persist($post);
        $this->entityManager->flush($post);
    }
}