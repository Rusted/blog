<?php

namespace AppBundle\Command\Command;

use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class PostCreateCommand
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 4, max = 255)
     * @Assert\Type(type="string")
     */
    public $title;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 20)
     * @Assert\Type(type="string")
     */
    public $body;

    /**
     * @var integer
     * @Assert\NotBlank()
     * @Assert\Choice(callback = {"AppBundle\Entity\Post", "getAvailableStatuses"})
     */
    public $status;

    /** @var User
     * @Assert\NotBlank()
     * @Assert\Type(type="AppBundle\Entity\User")
     */
    public $user;

}