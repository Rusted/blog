<?php

namespace AppBundle\Command\Exception;

class CommandHandlerException extends \Exception
{
    /**
     * @var string
     */
    protected $message;

    /**
     * @var array
     */
    protected $messages;

    /**
     * @param null $message
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        if (is_array($message)) {
            $this->message = implode(';', $message);
            $this->messages = $message;
        } else {
            $this->message = $message;
            $this->messages = array($message);
        }

        parent::__construct($this->message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}