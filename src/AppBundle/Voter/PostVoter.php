<?php

namespace AppBundle\Voter;

use AppBundle\Entity\Post;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class PostVoter extends Voter
{
    const POST_LIST = 'post.list';
    const POST_VIEW = 'post.view';
    const POST_CREATE = 'post.create';
    const POST_EDIT = 'post.edit';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    public function supports($attribute, $subject)
    {
        if (!is_null($subject )) {
            if (!$subject instanceof Post) {
                return false;
            }
        }

        return in_array($attribute, [
            self::POST_CREATE,
            self::POST_VIEW,
            self::POST_LIST,
            self::POST_EDIT
        ]);
    }

    /**
     * @param string $attribute
     * @param null|Post $subject
     * @param TokenInterface $token
     * @return bool
     */
    public function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        switch($attribute) {
            case self::POST_LIST:
            case self::POST_CREATE:
                return $token->getUser() instanceof UserInterface
                    && in_array('ROLE_USER', $token->getUser()->getRoles())
                ;
            
            case self::POST_VIEW:
                return $subject instanceof Post
                    && $subject->getStatus() == Post::STATUS_PUBLIC || ($subject->getAuthor() == $token->getUser())
                    && in_array('ROLE_USER', $token->getUser()->getRoles())
                ;
            case self::POST_EDIT:
                return $subject instanceof Post
                    && $subject->getAuthor() == $token->getUser()
                    && in_array('ROLE_USER', $token->getUser()->getRoles())
                ;
        }

        return false;
    }
}