<?php

namespace AppBundle\Form;

use AppBundle\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       $builder
           ->add(
               'title',
               'text'
           )
           ->add(
               'body',
               'textarea'
           )
           ->add(
               'status',
               'choice',
               [
                   'choices' => Post::getStatusNames()
               ]
           )
           ->add(
               'submit',
               'submit'
           )
       ;
    }
}
