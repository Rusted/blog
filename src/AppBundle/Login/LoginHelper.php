<?php
namespace AppBundle\Login;

use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class LoginHelper
{
    /** @var TokenStorage */
    private $tokenStorage;

    /** @var EncoderFactory */
    private $encoderFactory;

    /** @var UserRepository */
    private $userRepository;

    /**
     * LoginHelper constructor.
     * @param TokenStorage $securityContext
     * @param EncoderFactory $encoderFactory
     * @param UserRepository $userRepository
     */
    public function __construct(TokenStorage $tokenStorage, EncoderFactory $encoderFactory, UserRepository $userRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->encoderFactory = $encoderFactory;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $username
     * @param $password
     * @param $providerKey
     * @return bool
     */
    public function login($username, $password, $providerKey)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy([
            'username' => $username
        ]);

        if ($user instanceof User) {
            $encoder = $this->encoderFactory->getEncoder($user);
            $encodedPassword = $encoder->encodePassword($password, $user->getSalt());
            if ($user->getPassword() == $encodedPassword) {
                $token = new UsernamePasswordToken($user, $password, $providerKey, $user->getRoles());
                $this->tokenStorage->setToken($token);

                return $user;
            }
        }

        return false;
    }
}
