## Goal of application ##

This repository purpose is to experiment with different ways to handle complexity. For practical purpose we came up with imaginary project - blogging engine. 

This engine will have ability to create a blog post via 3 different inputs:

* website (filling in form)
* command line tool
* API call

Intention of this "project" is to simulate some complex situations where you need to check permissions and do some business logic and handle different kind of side effects of operation in clean way, to have concise, testable and predictable code.

### Blog post structure ###

* title
* body
* author
* status (draft|public; default - draft)

### User structure ###

* email
* password

### Business logic rules ###

* user cannot post more than 5 posts per day